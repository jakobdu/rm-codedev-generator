#!/bin/bash

# Script to generate the outline for the codedev part of the REMIND Meeting.
# Meant to be executed from a git parent directory with all repositories as
# subfolders using their default names (as given by git clone).
# To ensure full functionality this script is best executed on the cluster in
# /p/projects/remind/rm-codedev-generator
# Use the command line argument to supply the date after which the code changes
# should be listed.
# Example: bash generator.sh "27.09.2021"

# Results will be written to the following file
out="remind-meeting.out";

## Part 1: REMIND model

echo "h4. Code Development" > "$out"
echo "* REMIND" >> "$out"
cd REMIND;
git checkout --quiet develop
git pull --quiet https://github.com/remindmodel/remind.git develop
git log --merges --first-parent develop --after=$1 \
	--format="%aN%x09%ad%x09%s%x09%b" --date=format:"%a %b %d" | \
    sort -bt $'\t' -k 1,1 -k 2.5M -k 2.9n | \
    sed "s|\(.*\)\t\(.*\)\tMerge pull request #\([0-9]\+\).*\t\(.*\)|** \"#\3 \":https://github.com/remindmodel/remind/pull/\3 _\1 (\2)_ \4|" >> "../$out"
cd ..;

## Part 2: R packages (merges)

# Lists all *merges* that have been performed into master.

repos=( mrcommons mrremind mrdrivers remind2 remind edgeTransport quitte mip magclass lucode2 gms modelstats piamInterfaces piamMappings );

echo "* R packages" >> "$out"

for i in "${repos[@]}"; do
    echo "** $i" >> $out;
    if [ ! -d "$i" ]; then
      # Clone if $i does not exist.
      echo "Cloning ${i}..."
      git clone "https://github.com/pik-piam/$i.git"
    fi
    cd $i;
    # check out the default branch of origin, whatever it is called
    branch=$( git symbolic-ref refs/remotes/origin/HEAD | sed 's|refs/remotes/origin/||' )
    git checkout --quiet "$branch"
    git pull --quiet "https://github.com/pik-piam/$i.git" "$branch"
    git log --merges --first-parent "$branch" --after=$1 \
	    --format="%aN%x09%ad%x09%s%x09%b" --date=format:"%a %b %d" | \
	sort -bt $'\t' -k 1,1 -k 2.5M -k 2.9n | \
        egrep -v '\[pre-commit\.ci\]' | \
	sed "s|\(.*\)\t\(.*\)\tMerge pull request #\([0-9]\+\).*\t\(.*\)|*** \"#\3 \":https://github.com/pik-piam/$i/pull/\3 _\1 (\2)_ \4|" >> "../$out"
    cd ..;
done

## Part 3: R packages (commits)

# Lists all *commits* that have been pushed to master.
# This is only useful if changes have been pushed directly to master 
# without pull request and subsequent merge (not the standard way).

repos=(); # if you know of repos where direct commits have been performed fill them in here otherwise leave blank

for i in "${repos[@]}"; do
    echo "** $i (commits)" >> $out;
    cd $i;
    git checkout master
    git pull
    git log master --after=$1 --pretty=format:"*** \"%h %aN %ar\":https://github.com/pik-piam/$i/commit/%H %s"|tac >> ../$out
    cd ..;
done
