# rm-codedev-generator

A script to generate a list of code developments for the REMIND meeting from `git log`

## What it does

Descends into repository subfolders, updates them and generates a copy-paste-able list of PR titles for the REMIND meeting minutes file.

## Usage

Update the variables 
- `afterdate` to specify the time frame to collect PRs
- `out`, the filename for the output

Execute from a git parent directory with all repositories as subfolders
using their default names (as given by git clone)

## Known Issues

- every now and then, **line-breaks are missing**. Please correct upon pasting/fix code.
- author is the author of the merge commit on the *first-parent*, i.e., develop or master, but should be the author of the *second-parent*, i.e., the author of the last commit in the branch to-be-merged.
- non-default repository paths will stop the script from working.
- non-existing repository paths will stop the script from working.
